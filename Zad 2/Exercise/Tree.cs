﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Exercise
{

    public class MyList<T> : IEnumerable<T>
    {
        private List<T> L;
        public MyList()
        {
            L = new List<T>();
        }

        public void Add(T x)
        {
            L.Add(x);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return ((IEnumerable<T>)L).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<T>)L).GetEnumerator();
        }
    }


    public class Tree<T> : IEnumerable<T>
    {
        public T Value;
        private EnumeratorOrder OrderConst;
        public EnumeratorOrder Order
        {
            get
            {
                return OrderConst;
            }
            set
            {
                if (Enum.IsDefined(typeof(EnumeratorOrder), value))
                {
                    OrderConst = value;
                    foreach (Tree<T> child in Children)
                    {
                        child.Order = value;
                    }
                }
                else
                {
                    throw new ArgumentOutOfRangeException();
                }
            }
        }


        private MyList<Tree<T>> ChildrenConst;
        public MyList<Tree<T>> Children
        {
            get
            {
                return ChildrenConst;
            }
        }
        //public Tree<T> Parent;

        //public Tree(T _value, EnumeratorOrder _order, Tree<T> _parent = null)
        public Tree(T _value, EnumeratorOrder _order)
        {
            ChildrenConst = new MyList<Tree<T>>();
            Order = _order;
            Value = _value;
            //    Parent = _parent;
        }

        public void Add(T _value)
        {
            //    Children.Add(new Tree<T>(_value, Order, this));
            ChildrenConst.Add(new Tree<T>(_value, Order));
        }

        public void Add(Tree<T> _subtree)
        {
            //    _subtree.Parent = this;
            ChildrenConst.Add(_subtree);
            _subtree.Order = Order;
        }


        public IEnumerator<T> GetEnumerator()
        {
            return new TreeEnumerator<T>(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }

    }



    public class TreeEnumerator<T> : IEnumerator<T>
    {
        private LinkedList<Tree<T>> buff;
        private Tree<T> active;

        public TreeEnumerator(Tree<T> head)
        {
            buff = new LinkedList<Tree<T>>();
            buff.AddLast(head);
            active = null;

        }

        public T Current
        {
            get
            {
                if (active == null)
                {
                    throw new InvalidOperationException();
                }
                return active.Value;
            }
        }

        private object CurrentObject
        {
            get { return Current; }
        }

        object IEnumerator.Current
        {
            get { return CurrentObject; }
        }

        public bool MoveNext()
        {
            if (buff.Last != null)
            {

                if (active == null)
                {
                    active = buff.Last.Value;
                    buff.RemoveLast();
                }
                else
                {
                    if (active.Order == EnumeratorOrder.BreadthFirstSearch)
                    {
                        active = buff.First.Value;
                        buff.RemoveFirst();
                    }
                    else
                    {
                        active = buff.Last.Value;
                        buff.RemoveLast();
                    }
                }

                if (active.Order == EnumeratorOrder.BreadthFirstSearch)
                {
                    foreach (Tree<T> child in active.Children)
                    {
                        buff.AddLast(child);
                    }
                }
                else
                {
                    foreach (Tree<T> child in Enumerable.Reverse(active.Children))
                    {
                        buff.AddLast(child);
                    }
                }

                return true;
            }
            else
            {
                active = null;
                return false;
            }
        }

        public void Reset()
        {
            active = null;
        }

        // Implement IDisposable, which is also implemented by IEnumerator(T).
        private bool disposedValue = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            disposedValue = true;
        }

        ~TreeEnumerator()
        {
            Dispose(false);
        }
    }

}

