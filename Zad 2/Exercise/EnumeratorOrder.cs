﻿
namespace Exercise
{
    public enum EnumeratorOrder
    {
        BreadthFirstSearch = 1,
        DepthFirstSearch = 2
    }
}