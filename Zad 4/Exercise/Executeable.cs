﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;











namespace Exercise
{

    public class Executeable<T>
    {

        /*public static implicit operator Executeable<T>(Func<T,T> f)
        {
            return new Executeable<T>(f);
        }*/
        private Func<T, T> func;
        public Func<T,T> F
        {
            get
            {
                return func;
            }
        }
        public Executeable()
        {
            func = x => x;
        }
        public Executeable(Func<T, T> f)
        {
            func = f;
        }
        public virtual T execute(T x)
        {
            return func(x);
        }
    }
}
