﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise
{
    public class Composition<T> : Executeable<T>, IEnumerable<Executeable<T>>
    {
        private List<Func<T, T>> LF;
        public List<Func<T,T>> FunctionList
        {
            get
            {
                return LF;
            }
        }
        public Composition()
        {
            LF = new List<Func<T, T>>();
            LF.Add(x => x);
        }
        public Composition(Executeable<T> exe) : base()
        {
            LF = new List<Func<T, T>>();
            LF.Add(exe.F);
        }
        public Composition(Executeable<T> exe1, Executeable<T> exe2) : base()
        {
            LF = new List<Func<T, T>>();
            LF.Add(exe1.F);
            LF.Add(exe2.F);
        }
        public Composition(List<Executeable<T>> L) : base()
        {
            LF = new List<Func<T, T>>();
            foreach (Executeable<T> exe in L)
            {
                LF.Add(exe.F);
            }
        }
        public Composition(Composition<T> comp1, Composition<T> comp2) : base()
        {
            LF = new List<Func<T, T>>();
            foreach (Func<T, T> f in comp1.FunctionList)
            {
                LF.Add(f);
            }
            foreach (Func<T, T> f in comp2.FunctionList)
            {
                LF.Add(f);
            }
        }
        public void Add(Executeable<T> exe)
        {
            LF.Add(exe.F);
        }
        public T CompFunc(T x)
        {
            return x;
        }
        public override T execute(T x)
        {
            foreach (Func<T,T> f in LF)
            {
                x = f(x);
            }
            return x;
        }


        IEnumerator<Executeable<T>> IEnumerable<Executeable<T>>.GetEnumerator()
        {
            return new CompositionEnumerator<T>(this);
        }

        public IEnumerator GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }



    public class CompositionEnumerator<T> : IEnumerator<Executeable<T>>
    {
        private List<Func<T, T>> list;
        private List<Func<T, T>>.Enumerator active;
        public CompositionEnumerator(Composition<T> source)
        {
            list = source.FunctionList;
            active = list.GetEnumerator();
        }
        public Executeable<T> Current
        {
            get
            {
                return new Executeable<T>(active.Current);
            }
        }
        private object CurrentObject
        {
            get { return Current; }
        }

        object IEnumerator.Current
        {
            get { return CurrentObject; }
        }

        public bool MoveNext()
        {
            return active.MoveNext();
        }
        public void Reset()
        {
            active = list.GetEnumerator();
        }
        private bool disposedValue = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            disposedValue = true;
        }

        ~CompositionEnumerator()
        {
            Dispose(false);
        }
    }
}

