﻿using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Exercise;

namespace Zad_4
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void implementeInterfaces()
        {
            Composition<double> comp = new Composition<double>();
            Assert.IsInstanceOfType(comp, typeof(IEnumerable<Executeable<double>>));
            Assert.IsInstanceOfType(comp, typeof(Executeable<double>));
        }

        [TestMethod]
        public void composeOneFunction()
        {
            Executeable<int> linearfunction = new Executeable<int>(x => 2 * x + 10);
            Composition<int> comp = new Composition<int>(linearfunction);
            Assert.AreEqual(comp.execute(0), 10);
            Assert.AreEqual(comp.execute(2), 14);
            Assert.AreEqual(comp.execute(5), 20);
        }

        [TestMethod]
        public void composeTwoFunctions()
        {
            Executeable<int> linearfunction = new Executeable<int> (x => 2 * x + 5);
            Executeable<int> quadraticfunction = new Executeable<int>(x => x* x);

            Composition<int> comp = new Composition<int>(linearfunction, quadraticfunction);
            Assert.AreEqual(comp.execute(0), 25);
            Assert.AreEqual(comp.execute(2), 81);
            Assert.AreEqual(comp.execute(5), 225);

            comp = new Composition<int>(quadraticfunction, linearfunction);
            Assert.AreEqual(comp.execute(0), 5);
            Assert.AreEqual(comp.execute(2), 13);
            Assert.AreEqual(comp.execute(5), 55);

            comp = new Composition<int>(linearfunction, linearfunction);
            Assert.AreEqual(comp.execute(0), 15);
            Assert.AreEqual(comp.execute(2), 23);
            Assert.AreEqual(comp.execute(5), 35);

            comp = new Composition<int>(quadraticfunction, quadraticfunction);
            Assert.AreEqual(comp.execute(0), 0);
            Assert.AreEqual(comp.execute(2), 16);
            Assert.AreEqual(comp.execute(5), 625);
        }

        [TestMethod]
        public void composeThreeFunctions()
        {
            Executeable<int> identity = new Executeable<int>();
            Executeable<int> linearfunction = new Executeable<int> (x => x + 2);
            Executeable<int> cubicfunction = new Executeable<int>(x => x * x * x - 1);
            List<Executeable<int>> functions = new List<Executeable<int>>();
            functions.Add(identity);
            functions.Add(linearfunction);
            functions.Add(cubicfunction);
            Composition<int> comp = new Composition<int>(functions);
            Assert.AreEqual(comp.execute(0), 7);
            Assert.AreEqual(comp.execute(2), 63);
            Assert.AreEqual(comp.execute(1), 26);
        }

        [TestMethod]
        public void addingFunctionToComposition()
        {
            Composition<int> comp = new Composition<int>();
            Assert.AreEqual(comp.execute(0), 0);
            Assert.AreEqual(comp.execute(2), 2);
            comp.Add(new Executeable<int>(x => x + 2));
            Assert.AreEqual(comp.execute(0), 2);
            Assert.AreEqual(comp.execute(2), 4);
            comp.Add(new Executeable<int>(x => x * x * x));
            Assert.AreEqual(comp.execute(0), 8);
            Assert.AreEqual(comp.execute(2), 64);
        }

        public void compositionOfCompositions()
        {
            Executeable<int> linearfunction = new Executeable<int>(x => x + 2);
            Executeable<int> quadraticfunction = new Executeable<int>(x => 2 * x * x);
            Composition<int> comp1 = new Composition<int>(linearfunction, quadraticfunction);
            Composition<int> comp2 = new Composition<int>(quadraticfunction, linearfunction);
            Composition<int> compOfComps = new Composition<int>(comp1, comp2);
            Assert.AreEqual(compOfComps.execute(0), 130);
            Assert.AreEqual(compOfComps.execute(1), 650);
            Assert.AreEqual(compOfComps.execute(2), 2050);
        }
        public void checkIterator()
        {
            Executeable<int> identity = new Executeable<int>();
            Executeable<int> linearfunction = new Executeable<int>(x => 3 * x - 2);
            Executeable<int> quadraticfunction = new Executeable<int>(x => 2 * x * x - 5);
            Executeable<int> cubicfunction = new Executeable<int>(x => x * x * x + x * x + x + 1);
            List<Executeable<int>> functions = new List<Executeable<int>>();
            functions.Add(identity);
            functions.Add(linearfunction);
            functions.Add(quadraticfunction);
            functions.Add(cubicfunction);
            Composition<int> comp = new Composition<int>(functions);
            int[] tab0 = new int[] { 0, -2, -5, 1 };
            int[] tab1 = new int[] { 1, 1, -3, 4 };
            int[] tab2 = new int[] { 2, 4, 3, 15 };
            int i = 0;
            foreach(Executeable<int> f in comp)
            {
                Assert.AreEqual(f.execute(0), tab0[i++]);
                Assert.AreEqual(f.execute(1), tab1[i++]);
                Assert.AreEqual(f.execute(2), tab2[i++]);
            }           
        }

    }
}
/*

 
    
         
    

        int i = 0;
        for(Executeable<Integer> f : comp) {
            Assert.assertEquals((int)f.execute(0), tab0[i]);
            Assert.assertEquals((int)f.execute(1), tab1[i]);
            Assert.assertEquals((int)f.execute(2), tab2[i]);
            i++;      
        } 
    }
}


    */