﻿
namespace Exercise
{
    public class Importer
    {
        public Importer() { }
        private Data dataConst;
        public Data data
        {
            get
            {
                return dataConst;
            }
        }
        public void ImportData(Data d)
        {
            dataConst = d;
        }
    }

    public class TextImporter : Importer
    {
        public TextImporter() : base() { }
        public string ImportedText
        {
            get
            {
                return (data as TextData).Text;
            }
        }
    }

}


