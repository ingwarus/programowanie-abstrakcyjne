﻿
namespace Exercise
{
    public class Exporter
    {
        public Exporter(Data d)
        {
            data = d;
        }
        private Data data;
        public Data ExportData
        {
            get
            {
                return data;
            }
        }
    }
    
    public class TextExporter : Exporter
    {
        public TextExporter(string text) : base(new TextData(text)) { }
    }

}
