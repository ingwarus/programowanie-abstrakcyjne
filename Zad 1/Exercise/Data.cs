﻿
namespace Exercise
{
    public class Data { }
    
    public class TextData: Data
    {
        public TextData(string txt)
        {
            TextConst = txt;
        }
        private string TextConst;
        public string Text
        {
            get
            {
                string tmp = TextConst;
                TextConst = string.Empty;
                return tmp;
            }
        }
    }
}