﻿
namespace Exercise
{
    public abstract class DistributedModuleFactory
    {
        public DistributedModuleFactory() { }
        public abstract Data CreateData();
        public abstract Exporter CreateExporter();
        public abstract Importer CreateImporter();
    }

    public class DistributedModuleTextFactory : DistributedModuleFactory
    {
        private string txt;
        public DistributedModuleTextFactory(string s) : base()
        {
            txt = s;
        }
        public override Data CreateData()
        {
            return new TextData(txt);
        }
        public override Exporter CreateExporter()
        {
            return new TextExporter(txt);
        }
        public override Importer CreateImporter()
        {
            return new TextImporter();
        }
    }
}

