﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class MyList<T> : IEnumerable<T>
{
    private List<T> L;
    public MyList()
    {
        L = new List<T>();
    }

    public void Add(T x)
    {
        L.Add(x);
    }

    public IEnumerator<T> GetEnumerator()
    {
        return ((IEnumerable<T>)L).GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return ((IEnumerable<T>)L).GetEnumerator();
    }
}









namespace Exercise
{
    public class TripleList<T> : IEnumerable<T>
    {
        public bool haveValue;
        private T value;
        public T Value
        {
            get
            {
                return value;
            }
        }
        public TripleList<T> PreviousElement;
        public TripleList<T> MiddleElement;
        public TripleList<T> NextElement;
        public TripleList()
        {
            haveValue = false;
            PreviousElement = MiddleElement = NextElement = null;
            
        }
        public int Count()
        {
            int n;
            if (haveValue) n = 1; else n = 0;
            if (MiddleElement != null && MiddleElement.haveValue) n += 1;
            if (NextElement != null) n += NextElement.Count();
            return n;
        }
        private int CountRev()
        {
            int n;
            if (haveValue) n = 1; else n = 0;
            if (MiddleElement != null && MiddleElement.haveValue) n += 1;
            if (PreviousElement != null) n += PreviousElement.CountRev();
            return n;
        }
        public void Add(T v)
        {
            if (haveValue)
            {
                if(MiddleElement == null)
                {
                    MiddleElement = new TripleList<T>();
                    MiddleElement.Add(v);
                    MiddleElement.MiddleElement = this;
                }
                else
                {
                    if(NextElement == null)
                        NextElement = new TripleList<T>();
                    NextElement.Add(v);
                    NextElement.PreviousElement = this;
                }
            }
            else
            {
                haveValue = true;
                value = v;
            }

        }
        public void Add(TripleList<T> tmp)
        {
            foreach (T x in tmp)
                Add(x);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new TripleListEnumerator<T>(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }


    public class TripleListEnumerator<T> : IEnumerator<T>
    {

        private TripleList<T> active;
        private TripleList<T> head;
        public bool isMiddle;
        public TripleListEnumerator(TripleList<T> h)
        {
            active = null;
            head = h;
        }

        public T Current
        {
            get
            {
                if (active == null)
                {
                    throw new InvalidOperationException();
                }
                return active.Value;
            }
        }

        private object CurrentObject
        {
            get { return Current; }
        }

        object IEnumerator.Current
        {
            get { return CurrentObject; }
        }

        public bool MoveNext()
        {
            if(active == null)
            {
                if (head.haveValue)
                {
                    active = head;
                    isMiddle = false;
                    return true;
                }
                else return false;
            }
            if(active.MiddleElement == null) return false;
            if(!isMiddle && active.MiddleElement != null)
            {
                active = active.MiddleElement;
                isMiddle = true;
                return true;
            }
            else
                if(active.MiddleElement.NextElement!= null)
                {
                    active = active.MiddleElement.NextElement;
                    isMiddle = false;
                    return true;
                }
            return false;
        }

        public void Reset()
        {
            active = null;
        }

        // Implement IDisposable, which is also implemented by IEnumerator(T).
        private bool disposedValue = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            disposedValue = true;
        }

        ~TripleListEnumerator()
        {
            Dispose(false);
        }
    }

}



/*


*/
