﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Exercise
{
    public class SortedDeque<T> : IEnumerable<T> where T : IComparable
    {
        private int maxSize;
        public bool Empty
        {
            get
            {
                return size == 0;
            }
        }
        private int size;
        private int uniqueSize;
        public int Size
        {
            get
            {
                return size;
            }
        }
        public int UniqueSize
        {
            get
            {
                return uniqueSize;
            }
        }
        public int MaxSize
        {
            get
            {
                return maxSize;
            }
        }
        private int dSize;



        private List<List<T>> L;
        public List<List<T>> Lista
        {
            get
            {
                return L;
            }
        }
        
        private List<List<int>> counter;
        public List<List<int>> Licznik
        {
            get
            {
                return counter;
            }
        }

        public SortedDeque(int ms, int ks) : base()
        {
            maxSize = ms;
            dSize = ks;
            size = uniqueSize = 0;
            counter = new List<List<int>>((maxSize + dSize - 1) / dSize);
            L = new List<List<T>>((maxSize + dSize - 1) / dSize);
        }

        public T PopFront()
        {
            if (size == 0) throw new InvalidOperationException();
            T tmp = L[0][0];
            --size;
            if((--counter[0][0])==0)
            {
                --uniqueSize;
                L[0].RemoveAt(0);
                counter[0].RemoveAt(0);
                if(L[0].Count==0)
                {
                    L.RemoveAt(0);
                }
            }
            return tmp;
        }
        public T PopBack()
        {
            if (L.Count == 0) throw new InvalidOperationException();
            T tmp = L.Last().Last();
            --size;
            if (--counter[L.Count-1][counter[L.Count - 1].Count-1] == 0)
            {
                --uniqueSize;
                L[L.Count - 1].RemoveAt(counter[L.Count - 1].Count - 1);
                counter[L.Count-1].RemoveAt(counter[L.Count - 1].Count - 1);
                if (L[L.Count - 1].Count == 0)
                {
                    L.RemoveAt(L.Count - 1);
                }
            }
            return tmp;
        }

        public void Push(T x)
        {
            if (uniqueSize == maxSize)
            {
                throw new InsufficientMemoryException();
            }
            ++size; ++uniqueSize;
            if (L.Count == 0)
            {
                PushNew(x, 0);
                return;
            }
            int maxi = -1;
            for (int i = 0; i < L.Count; ++i)
            {
                if (x.CompareTo(L[i].First()) >= 0)
                    maxi = i;
                else
                    break;
            }


            

            if (maxi < 0)
                PushIn(x, maxi + 1);
            else
                PushIn(x, maxi);
        }

        private void PushIn(T x, int i)
        {
            List<T> D = L[i];
            int j;
            for (j = 0; j < D.Count && x.CompareTo(D[j]) >= 0; ++j)
            {
                
                if (x.Equals(D[j]))
                {
                    ++counter[i][j]; --uniqueSize;
                    return;
                }
            }
            if (D.Count < dSize)
            {
                PushDirect(x, i);
                return;
            }
            if (i + 1 < L.Count && L[i + 1].Count < dSize)
            {
                if (x.CompareTo(D.Last()) > 0)
                {
                    PushDirect(x, i + 1);
                }
                else
                {
                    PushDirect(D.Last(), i + 1, counter[i].Last());
                    D.RemoveAt(D.Count - 1);
                    counter[i].RemoveAt(D.Count);
                    PushDirect(x, i);
                }
                return;
            }
            if (L.Count < L.Capacity)
            {
                if (x.CompareTo(D.Last()) > 0)
                {
                    PushNew(x, i + 1);
                }
                else
                {
                    PushNew(D.Last(), i + 1, counter[i].Last());
                    D.RemoveAt(D.Count - 1);
                    counter[i].RemoveAt(D.Count);
                    PushDirect(x, i);
                }
                return;
            }
            for (j = 0; j < L.Count && L[j].Count == dSize; ++j) ;
            if (j < i)
                PushLeft(x, i);
            else
                PushRight(x, i);
        }

        private void PushLeft(T x, int i, int n = 1)
        {
            if (L[i].Count == dSize)
                PushLeft(L[i].First(), i - 1, counter[i][0]);
            L[i].RemoveAt(0);
            counter[i].RemoveAt(0);
            PushDirect(x, i, n);
        }
        private void PushRight(T x, int i, int n = 1)
        {
            if (L[i].Count == dSize)
                PushRight(L[i].Last(), i + 1, counter[i][L[i].Count - 1]);
            L[i].RemoveAt(L[i].Count - 1);
            counter[i].RemoveAt(L[i].Count - 1);
            PushDirect(x, i, n);
        }
        private void PushDirect(T x, int i, int n = 1)
        {
            int j;
            for (j = 0; j < L[i].Count && x.CompareTo(L[i][j]) > 0; ++j) ;
            L[i].Insert(j, x);
            counter[i].Insert(j, n);
        }
        private void PushNew(T x, int i, int n = 1)
        {
            L.Insert(i, new List<T>(dSize));
            L[i].Add(x);
            counter.Insert(i, new List<int>(dSize));
            counter[i].Add(n);
        }




        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return new SortedDequeEnumerator<T>(this);
        }

        public IEnumerator GetEnumerator()
        {
            return new SortedDequeEnumerator<T>(this);
        }
    }



    public class SortedDequeEnumerator<T> : IEnumerator<T> where T : IComparable
    {
        private List<List<T>> L;
        private List<List<int>> counter;
        private int i;
        private int j;
        private int k;
        public SortedDequeEnumerator(SortedDeque<T> source)
        {
            L = source.Lista;
            counter = source.Licznik;
            Reset();
        }
        public T Current
        {
            get
            {
                return L[i][j];
            }
        }
        private object CurrentObject
        {
            get { return Current; }
        }

        object IEnumerator.Current
        {
            get { return CurrentObject; }
        }

        public bool MoveNext()
        {
            if (i >= L.Count) return false;
            if (j >= L[i].Count) return false;
            ++k;
            if(k>=counter[i][j])
            {
                k = 0;
                ++j;
                if(j>=L[i].Count)
                {
                    j = 0;
                    ++i;
                    if(i>=L.Count)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        public void Reset()
        {
            k = -1;
            i = j = 0;
        }
        private bool disposedValue = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            disposedValue = true;
        }

        ~SortedDequeEnumerator()
        {
            Dispose(false);
        }
    }
}
