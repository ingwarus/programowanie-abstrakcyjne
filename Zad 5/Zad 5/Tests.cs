﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Exercise;
namespace Tests
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void Empty()
        {
            SortedDeque<int> deque = new SortedDeque<int>(30, 3);
            Assert.AreEqual(0, deque.Size);
            Assert.AreEqual(0, deque.UniqueSize);
            Assert.IsTrue(deque.Empty);
            Assert.AreEqual(30, deque.MaxSize);

        }
        
        [TestMethod]
        public void Add()
        {
            SortedDeque<int> deque = new SortedDeque<int>(30, 3);
            int[] tab =
            {
                1, 2, 3, 4, 1, 3, 5, 2, 6, 2, 5, 1, 6,9,1,4,2,6,8,-22,-1,-1,3,4,7,
            };
            foreach (int i in tab)
            {
                deque.Push(i);
            }
            Array.Sort(tab);
            int k = 0;
            foreach (int i in deque)
            {
                Assert.AreEqual(tab[k++], i);
            }
        }
        
        
        [TestMethod]
        public void TestEnumerate()
        {
            SortedDeque<int> deque = new SortedDeque<int>(30, 10);
            int[] tab =
           {
                1, 2, 3, 4, 1, 3, 5, 2, 6, 2, 5, 1, 6,9,1,4,2,6,8,-22,-1,-1,3,4,7,
            };
            Array.Sort(tab);
            int i = 0;
            foreach (var item in deque)
            {
                Assert.AreEqual(tab[i++], item);
            }
        }
        [TestMethod]
        [ExpectedException(typeof(InsufficientMemoryException))]
        public void AddToFull()
        {
            SortedDeque<int> deque = new SortedDeque<int>(10, 2);
            int[] tab =
            {
                1, 2, 3, 4, 5, 6, 8, 9, 0, 10, 11, 12, 13
            };
            foreach (var i in tab)
                deque.Push(i);
        }

        [TestMethod]
        [ExpectedException(typeof(InsufficientMemoryException))]
        public void ZeroMemory()
        {
            SortedDeque<int> deque = new SortedDeque<int>(0, 2);
            deque.Push(1);
        }

        [TestMethod]
        public void Count()
        {
            SortedDeque<int> deque = new SortedDeque<int>(30, 3);
            int[] tab =
            {
                1,1,1,1, 2,2,2,2, 3,3, 4,4, 5,5, 6,6, 9
            };
            foreach (int i in tab)
            {
                deque.Push(i);
            }
            Assert.AreEqual(tab.Length, deque.Size);
            Assert.AreEqual(7, deque.UniqueSize);
        }
        
        [TestMethod]
        public void PopBack()
        {
            SortedDeque<int> deque = new SortedDeque<int>(30, 2);
            int[] tab =
            {
               1, 2, 3, 4, 1, 3, 5, 2, 6, 2, 5, 1
            };
            foreach (var i in tab)
            {
                deque.Push(i);
            }
            Assert.AreEqual(6, deque.PopBack());
            Assert.AreEqual(5, deque.PopBack());
            Assert.AreEqual(5, deque.PopBack());
            Assert.AreEqual(9, deque.Size);
            Assert.AreEqual(4, deque.UniqueSize);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void PopEmptyFront()
        {
            SortedDeque<int> deque = new SortedDeque<int>(30, 2);
            deque.PopFront();

        }
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void PopEmptyBack()
        {
            SortedDeque<int> deque = new SortedDeque<int>(30, 2);
            deque.PopBack();

        }
        [TestMethod]
        public void PopFront()
        {
            SortedDeque<int> deque = new SortedDeque<int>(30, 2);
            int[] tab =
            {
               1, 2, 3, 4, 1, 3, 5, 2, 6, 2, 5, 1
            };
            foreach (var i in tab)
            {
                deque.Push(i);
            }
            Assert.AreEqual(1, deque.PopFront());
            Assert.AreEqual(1, deque.PopFront());
            Assert.AreEqual(1, deque.PopFront());
            Assert.AreEqual(2, deque.PopFront());
            Assert.AreEqual(2, deque.PopFront());
            Assert.AreEqual(2, deque.PopFront());
            Assert.AreEqual(6, deque.Size);
            Assert.AreEqual(4, deque.UniqueSize);
        }
        
    }
}